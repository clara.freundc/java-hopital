public class Nurse extends MedicalStaff {

    // Constructeur
    public Nurse(String employeeId, String name, int age, String socialSecurityNumber) {
        super(employeeId, name, age, socialSecurityNumber);
    }

    // Méthodes
    // override pour réécrire la méthode du parent
    @Override
    public void careForPatient(Patient patient) {
        System.out.println("Nurse " + this.name + " cares for " + patient.name);
    }

    @Override
    public void getRole() {
        System.out.println(this.name + " is a Nurse");
    }
}