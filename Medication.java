public class Medication {

    // Paramètres
    String name;
    String dosage;

    // Constructeur
    public Medication(String name, String dosage) {
        this.name = name;
        this.dosage = dosage;
    }

    // Méthodes
    public String getInfo() {
        return name + ' ' + dosage;
    }
}
