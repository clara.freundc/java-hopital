import java.util.ArrayList;

public class Illness {

    // Paramètres
    private String name;
    private ArrayList<Medication> medicationList;

    // Constructeur 
    public Illness(String name) {
        this.name = name;
        this.medicationList = new ArrayList<>();
    }

    // Méthodes :
    public String getInfo() {
        String result = "";

        for (Medication medication: medicationList) {
            result += medication.getInfo();
        }
        return ' ' + this.name + ' ' + result;
    }

    public void addMedication(Medication medication) {

        this.medicationList.add(medication);
        System.out.println("The following medicine was added to the list : " + medication.getInfo() + " for this disease : " + this.name);
    }
}
