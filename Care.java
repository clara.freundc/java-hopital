interface Care {

    void careForPatient(Patient patient);

    // on rajoute default pour que la méthode accepte une valeur par défaut
    default void recordPatientVisit(String notes) {
        System.out.println(notes);
    }
} 
