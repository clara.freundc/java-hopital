public abstract class MedicalStaff extends Person implements Care {

    // Paramètres
    private String employeeId;

    // Constructeur sinon erreur
    public MedicalStaff(String employeeId, String name, int age, String socialSecurityNumber) {
        super(name, age, socialSecurityNumber);
        this.employeeId = employeeId;
    }

    
    // Méthodes
    public abstract void getRole();
}
