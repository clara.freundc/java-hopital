import java.util.ArrayList;

public class Hospital {

    public static void main(String[] args) {

        // Instances de médicament
        Medication morphine = new Medication("Morphine", "150 mg, 3 days");
        Medication sommeil = new Medication("Sleep", "Once per day, every day until you die");
        Medication doliprane = new Medication("Doliprane", "Once per mmonth");
        Medication videoGame = new Medication("Video Game", "3 hours per day, for 42 years minimum");
        // Test de médicaments
        // System.out.println(morphine.getInfo());
        // System.out.println(sommeil.getInfo());
        // System.out.println(doliprane.getInfo());
        // System.out.println(videoGame.getInfo());

        // Instances de maladies
        Illness stress = new Illness("Stress");
        Illness covid = new Illness("Covid");
        Illness no = new Illness("Cant say no");
        // Test de maladies
        // stress.addMedication(videoGame);
        // stress.addMedication(sommeil);
        // covid.addMedication(doliprane);
        // no.addMedication(videoGame);
        // System.out.println(stress.getInfo());
        // System.out.println(covid.getInfo());
        // System.out.println(no.getInfo());


        // Instances de Patient
        Patient evan = new Patient("1", "Evan", 23, "2345");
        // Test de Patient
        // evan.addIllness(stress);
        // evan.addIllness(no);
        // System.out.println(evan.getInfo());

        // Instances de Doctor
        Doctor loic = new Doctor("sos", "1", "Loïc", 25, "1234");
        // Tests de Doctor
        // loic.getRole();
        // loic.careForPatient(evan);

        // Instances de Nurse
        Nurse oskour = new Nurse("2", "Oskour", 0, "666");
        // Test de Nurse
        // System.out.println(oskour.name + ' ' + oskour.socialSecurityNumber);
        // oskour.getName();
        // oskour.getRole();
        // oskour.careForPatient(evan);
    }
}
