public class Doctor extends MedicalStaff {

    // Paramètres
    private String specialty;

    // Constructeur
    public Doctor(String specialty, String employeeId, String name, int age, String socialSecurityNumber) {
        super(employeeId, name, age, socialSecurityNumber);
        this.specialty = specialty;
    }

    // Méthodes
    // override pour réécrire la méthode du parent
    @Override
    public void careForPatient(Patient patient) {
        System.out.println("Doctor " + this.name + " cares for " + patient.name);
    }

    @Override
    public void getRole() {
        System.out.println(this.name + " is a Doctor");
    }
}