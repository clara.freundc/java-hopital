import java.util.ArrayList;

public class Patient extends Person {

    // Paramètres
    private String patientId;
    private ArrayList<Illness> illnessList;

    // Constructeur
    public Patient(String patientId, String name, int age, String socialSecurityNumber) {
        super(name, age, socialSecurityNumber);
        this.patientId = patientId;
        this.illnessList = new ArrayList<>();
    }

    // Méthodes
    public String getInfo() {

        String result = "";

        for (Illness illness : illnessList) {
            result += illness.getInfo();
        }
        return "patient ID : " + this.patientId + ", name : " + this.name + ", age : " + this.age + ", diagnosis : " + result  + ", social security number : " + this.socialSecurityNumber;
    }

    public void addIllness(Illness illness) {
        this.illnessList.add(illness);
        System.out.println("The following disease : " + illness.getInfo() + "has been diagnosed for patient : " + this.name + " (" + this.patientId + ")");
    }
}
