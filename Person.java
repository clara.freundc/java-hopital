public abstract class Person {

    // Paramètres
    String name;
    int age;
    String socialSecurityNumber;

    // Constructeur 
    public Person(String name, int age, String socialSecurityNumber) {
        this.name = name;
        this.age = age;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    // Méthodes getter
    public String getName() {
        return name;
    }
    public int getAge() {
        return age;
    }
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    } 
}
